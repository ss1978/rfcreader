function $(arg) { return document.getElementById(arg); }
function $element(arg, par) { var n = document.createElement(arg); if (par) par.appendChild(n); return n; }

window.addEventListener("load", function() {
	refreshIndex();
	$("search-rfc").onchange = search;
	$("reset").onclick = function() { window.localStorage.clear(); window.close(); };
	$("switch-search").onclick = function() { $("search").style.display=""; };
});

var prevY=0;
window.addEventListener("scroll", function(e) {
	var diff = e.pageY-prevY;
	if (e.pageY<100 || diff<-100){
			$("header").className="";
	}
	else if (e.pageY>100 && diff>0){
		$("header").className="hidden";
	}
	prevY=e.pageY;
	
	return true;
});

function display( data ) {
		$("download-index").style.display="none";
		$("search").style.display="none";
		$("view").textContent = data.text;
		$("view").style.display="";
	return data;
}

function selector( rfcno ) {
	return function view(e )
	{
		var data = retrieve(rfcno);
		if (data==null)
			download(rfcno);
		else
			display(data);
	};
}

function download( rfcId ) {
	var request = new XMLHttpRequest({mozSystem:true});
	request.onload = function onload( e ){
		console.info("Downloaded",e,request);
		var data = store(display(newData(request, {"title":"", "id":rfcId})));
		var elem = $("rfc-" + rfcId);
		if (elem) elem.className = "available";
	}
	var url = "http://tools.ietf.org/rfc/rfc"+Number.parseInt(rfcId)+".txt";
	request.open("GET", url, true);
	request.send();
	$("download-index").style.display="none";
	$("search").style.display="";
}

function newResult(record, results) {
	var res = document.importNode($("result-row").content, true );
	var cells = res.childNodes[0].cells;
	var matches = record.match(/\d+/);
	if (matches!=null){
		
		var rfcno = matches[0];
		if (results[rfcno]==null){			
			cells.item(0).textContent = rfcno;
			cells.item(1).textContent = record.substring(5);
			res.childNodes[0].onclick = selector(rfcno);
			if (available(rfcno)) res.childNodes[0].className="available";
			res.childNodes[0].setAttribute("id","rfc-"+rfcno);
	
			$("result-list").appendChild(res.childNodes[0]);
			results[rfcno]=true;
		}
	}
}

function clearResults() {
	var resultList = $("result-list");
	while (resultList.childElementCount>0) resultList.removeChild(resultList.firstChild);
}

function searchIndex(content) {
	var index = retrieve("0");
	var indexPos = -1;
	clearResults();
	var results = {};
	while ((indexPos = index.text.indexOf(content, indexPos+content.length))>0) {		
		var startP = index.text.lastIndexOf("\n\n", indexPos);
		var endP = index.text.indexOf("\n\n", indexPos );
		if (startP>0 && endP>0) {
			newResult(index.text.substring( startP, endP ).trim(), results);			
		}			
	}
}

function search() {
	$("in-progress").style.display="";
	searchIndex( $("search-rfc").value );
	$("in-progress").style.display="none";
	return false;
}

function available( key ) {
	return window.localStorage.getItem(key+"-modified") != null;
}

function retrieve( key ) {
	var modified = window.localStorage.getItem(key+"-modified");
	if (modified!=null)
		return {"modified": modified, "text": window.localStorage.getItem(key+"-text")};
	else
		return null;
}

function store( data ) {
	window.localStorage.setItem( data.id + "-modified", data.modified);
	window.localStorage.setItem( data.id + "-text", data.text);
	return data;
}

function newData(request, skeleton) {
	skeleton.modified = request.getResponseHeader("Last-Modified");
	skeleton.text = request.responseText;
	return skeleton;
}

function downloadIndex() {
	console.info("Starting download");
	var request = new XMLHttpRequest({mozSystem:true});
	request.onload = function onload( e ){
		console.info("Downloaded",e,request);
		store(newData(request, {"title":"index", "id":0}));
		$("download-index").style.display="none";
		$("search").style.display="";
	}
	request.open("GET", "http://www.ietf.org/download/rfc-index.txt", true);
	request.send();
}

function refreshIndex() {
	$("download-index").style.display="";
	$("search").style.display="none";
	console.info("Checking index availability.");
	var index = retrieve("0");
	if (index == null) {
		downloadIndex();
	}
	else {
		$("download-index").style.display="none";
		$("search").style.display="";
		
		console.info("Index found:", index.modified);
	}
}
